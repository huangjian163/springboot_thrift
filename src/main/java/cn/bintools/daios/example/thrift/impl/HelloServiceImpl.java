package cn.bintools.daios.example.thrift.impl;

import cn.bintools.daios.example.thrift.ConnectionInfo;
import cn.bintools.daios.example.thrift.HelloService;
import org.apache.thrift.TException;

/**
 * 服务端实现
 *
 * @author <a href="jian.huang@bintools.cn">yunzhe</a>
 * @version 1.0.0 2019-07-02-下午7:14
 */
public class HelloServiceImpl implements HelloService.Iface {
    @Override
    public int add(int num1, int num2) throws TException {
        return num1+num2;
    }

    @Override
    public ConnectionInfo getConnInfoById(int cpId) throws TException {
        ConnectionInfo connectionInfo = new ConnectionInfo();
        connectionInfo.setConnId(12);
        connectionInfo.setConnectionName("ThrfaceiftConnName");
        connectionInfo.setUrl("192.168.1.162");
        connectionInfo.setPort(3306);
        connectionInfo.setUserName("mysql_conn");
        connectionInfo.setPassword("abc*&ABC123");
        return connectionInfo;
    }
}